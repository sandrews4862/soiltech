//
//  STButton.swift
//  SoilTech
//
//  Created by Steven Andrews on 2019-09-18.
//  Copyright © 2019 Steven Andrews. All rights reserved.
//

import UIKit

class STButton: UIButton {

   override init(frame: CGRect) {
    super.init(frame: frame)
    //setup()
  }

   required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    //setup()
  }

//    func setup() {
//        self.clipsToBounds = true
//        self.layer.cornerRadius = 8
//        self.backgroundColor = UIColor(named: "brand")
//        self.setTitleColor(UIColor.white, for: .normal)
//        self.titleLabel?.font = UIFont.init(name: "Hevetica", size: 20)
//    }
//
//    override open var isHighlighted: Bool {
//        didSet {
//            layer.opacity = isHighlighted ? 0.5 : 1
//        }
//    }
}
