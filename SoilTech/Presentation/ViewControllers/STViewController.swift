//
//  STViewController.swift
//  SoilTech
//
//  Created by Steven Andrews on 2019-09-20.
//  Copyright © 2019 Steven Andrews. All rights reserved.
//

import UIKit

class STViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let backButton =  UIBarButtonItem(title: "Back", style: .plain, target: nil, action: nil)
        self.navigationItem.backBarButtonItem = backButton;
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
