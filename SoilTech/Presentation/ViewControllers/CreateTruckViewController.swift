//
//  CreateTruckViewController.swift
//  SoilTech
//
//  Created by Jessica Andrews on 2019-09-27.
//  Copyright © 2019 Steven Andrews. All rights reserved.
//

import UIKit

protocol CreateTruckViewControllerDelegate {
    func truckCreated(truck: STTruck)
}

class CreateTruckViewController: UIViewController {

    @IBOutlet private var backgroundView: UIView!
    @IBOutlet private var createButton: UIButton!
    @IBOutlet private var numberField: UITextField!
    @IBOutlet private var plateField: UITextField!
    @IBOutlet private var runField: UITextField!
    
    public var sheet: STSheet?
    public var delegate: CreateTruckViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        numberField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        plateField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        runField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        backgroundView.layer.cornerRadius = 8
        createButton.isEnabled = false
        
        numberField.becomeFirstResponder()
    }
    
    private func canCreate() -> Bool {
        return numberField.text?.isEmpty != true && plateField.text?.isEmpty != true
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        createButton.isEnabled = canCreate()
    }
    
    @IBAction private func createPressed() {
        guard canCreate() == true, let number = numberField.text, let plate = plateField.text else {
            return
        }
        var run: Int? = nil
        if let runString = runField.text {
            run = Int(runString)
        }
        TruckService.createTruck(number: number, plate: plate, sheet: sheet!, response: { [weak self] (newTruck) in
            if let truck = newTruck as? STTruck {
                if let run = run {
                    TruckService.createRun(truck: truck, run: run, response: { (run) in
                        self?.delegate?.truckCreated(truck: truck)
                        self?.dismiss(animated: true, completion: nil)
                    }) { (fault) in
                        print("Error creating run: \(fault)")
                    }
                }
                self?.delegate?.truckCreated(truck: truck)
                self?.dismiss(animated: true, completion: nil)
            } else {
                print("Something went wrong while creating the truck!")
            }
        }) { (fault) in
            print("Error creating truck: \(fault)")
        }
    }
    
    @IBAction private func closePressed() {
        dismiss(animated: true, completion: nil)
    }

}

extension CreateTruckViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == numberField {
            plateField.becomeFirstResponder()
        } else if textField == plateField {
            runField.becomeFirstResponder()
        } else if textField == runField {
            createPressed()
        }
        return true
    }
}
