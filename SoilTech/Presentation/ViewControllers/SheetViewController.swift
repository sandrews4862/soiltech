//
//  SheetViewController.swift
//  SoilTech
//
//  Created by Jessica Andrews on 2019-09-22.
//  Copyright © 2019 Steven Andrews. All rights reserved.
//

import UIKit
import SVProgressHUD

class SheetViewController: UIViewController {

    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var loader: UIActivityIndicatorView!
    @IBOutlet var emptyView: UIView!
    @IBOutlet var errorLabel: UILabel!
    
    public var company: STCompany!
    private var SEARCH_DELAY = 0.3
    private var sheet: STSheet? {
        didSet {
            if let site = sheet?.site {
                navigationItem.title = site
            }
        }
    }
    private var trucks: [STTruck] = []
    private var searchTimer: Timer?
    private var filterSet: [STTruck] = []
    private var selectedTruck: STTruck!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = ""
        
        tableView.register(UINib(nibName: "TruckTableViewCell", bundle: nil), forCellReuseIdentifier: "truckTableViewCell")
        tableView.register(TruckTableViewHeader.self, forHeaderFooterViewReuseIdentifier: "truckTableViewHeader")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        reloadFullSheet()
    }
    
    private func reloadSheet() {
        loader.startAnimating()
        CompanyService.fetchMostRecentSheet(companyId: company.objectId, response: { [weak self] (sheets) in
            if let sheets = sheets as? [STSheet], let sheet = sheets.first {
               let calendar = Calendar.current
               if let created = sheet.created as Date?, calendar.isDateInToday(created) == false {
                    self?.showCreateSheet(sheet.site)
                } else {
                self?.sheet = sheet
                self?.reloadFullSheet()
                }
            } else {
                self?.showCreateSheet(nil)
            }
            self?.tableView.reloadData()
        }) { [weak self] (fault) in
            self?.loader.stopAnimating()
            if fault.debugDescription.contains("object store is empty") {
                self?.showCreateSheet(nil)
            } else {
                self?.loader.stopAnimating()
                print("Error fetching sheets: \(fault.debugDescription)")
                self?.tableView.reloadData()
            }
        }
    }
    
    private func reloadFullSheet() {
        if (sheet == nil) {
            reloadSheet()
            return
        }
        trucks.removeAll()
        filterSet.removeAll()
        loader.startAnimating()
        CompanyService.fetchSheet(sheetId: sheet!.objectId, response: { [weak self] (sheet) in
            if let sheet = sheet as? STSheet {
                self?.loader.stopAnimating()
                self?.sheet = sheet
                self?.trucks = sheet.trucks ?? []
                self?.filterSet = sheet.trucks ?? []
            }
            self?.tableView.reloadData()
        }) { [weak self] (fault) in
            self?.loader.stopAnimating()
            print("Error fetching sheets: \(fault.debugDescription)")
            self?.tableView.reloadData()
        }
    }
    
    private func showCreateSheet(_ lastSite: String?) {
        loader.stopAnimating()
        let alert = UIAlertController(title: "New Sheet", message: "Enter the site name", preferredStyle: .alert)

        alert.addTextField { (textField) in
            textField.placeholder = "Site"
            textField.text = ""
        }

        alert.addAction(UIAlertAction(title: "Create", style: .default, handler: { [weak alert] (_) in
            let textField = alert?.textFields![0]
            if let name = textField?.text, name.isEmpty == false, name != "" {
                CompanyService.createSheet(site: name, company: self.company, response: { [weak self] (company) in
                    self?.showSuccessAlert()
                    self?.reloadFullSheet()
                }) { (fault) in
                    print("Error creating sheet: \(fault.debugDescription)")
                }
            }
        }))
        if let lastSite = lastSite {
            alert.addAction(UIAlertAction(title: "Use \(lastSite)", style: .default, handler: { [weak self] (_) in
                guard let company = self?.company else {
                    return
                }
                CompanyService.createSheet(site: lastSite, company: company, response: { (sheet) in
                    self?.showSuccessAlert()
                }) { (fault) in
                    print("Error creating sheet: \(fault.debugDescription)")
                }
            }))
        }
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { [weak self] (_) in
            self?.navigationController?.popViewController(animated: true)
        }))

        self.present(alert, animated: true, completion: nil)
    }
    
    private func showSuccessAlert() {
        reloadFullSheet()
        let alert = UIAlertController(title: "Success", message: "The sheet has been created!", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showAddTruck" {
            let destVC = segue.destination as! CreateTruckViewController
            destVC.delegate = self
            destVC.sheet = sheet
        } else if segue.identifier == "showTruck" {
            let destVC = segue.destination as! TruckViewController
            destVC.truck = selectedTruck
        }
    }
    
}

extension SheetViewController: CreateTruckViewControllerDelegate {
    func truckCreated(truck: STTruck) {
        reloadFullSheet()
    }
}

extension SheetViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filterSet = trucks.filter({ (truck) -> Bool in
            return truck.plate?.lowercased().contains(searchText.lowercased()) ?? false || truck.truckNumber?.lowercased().contains(searchText.lowercased()) ?? false
        })

        if searchText.isEmpty || searchText == "" {
            emptyView.isHidden = true
            errorLabel.isHidden = true
            filterSet = trucks
        }
        tableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    private func showErrorAlert(_ message: String) {
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    private func showCreateRun(_ truck: STTruck) {

        let alert = UIAlertController(title: "New Run", message: nil, preferredStyle: .alert)

        alert.addTextField { (textField) in
            textField.placeholder = "Run Value"
            textField.text = ""
            textField.keyboardType = .numberPad
        }

        alert.addAction(UIAlertAction(title: "Create", style: .default, handler: { [weak alert] (_) in
            let textField = alert?.textFields![0]
            if let run = textField?.text, run.isEmpty == false, run != "", let runInt = Int(run) {
                self.loader.startAnimating()
                TruckService.createRun(truck: truck, run: runInt, response: { [weak self] (run) in
                    self?.reloadFullSheet()
                }) { (fault) in
                    self.loader.stopAnimating()
                    self.showErrorAlert("Error creating the run! Please try again.")
                    print("Error creating run: \(fault.debugDescription)")
                }
            } else {
                self.showErrorAlert("Invalid data entered. Use numerical values only.")
            }
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))

        self.present(alert, animated: true, completion: nil)
    }
    
}

extension SheetViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filterSet.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "truckTableViewCell", for: indexPath) as? TruckTableViewCell {
            let truck = filterSet[indexPath.row]
            cell.update(number: truck.truckNumber ?? "", plate: truck.plate ?? "", runs: truck.runs?.count ?? 0, delegate: self)
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "truckTableViewHeader") as? TruckTableViewHeader {
            return header
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        showCreateRun(filterSet[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let action = UIContextualAction(style: .destructive, title: "Delete",
          handler: { [weak self] (action, view, completionHandler) in
            self?.loader.startAnimating()
            let truck = self!.filterSet[indexPath.row]
            
            TruckService.deleteTruck(truck: truck, response: { [weak self] (response) in
                self?.reloadFullSheet()
            }) { (fault) in
                self?.loader.stopAnimating()
                print("Error deleting run: \(fault.debugDescription)")
            }
            completionHandler(true)
        })
        let configuration = UISwipeActionsConfiguration(actions: [action])
        return configuration
    }
}

extension SheetViewController: TruckTableViewCellDelegate {
    
    func selectedListView(cell: TruckTableViewCell) {
        if let indexPath = tableView.indexPath(for: cell) {
            selectedTruck = filterSet[indexPath.row]
            performSegue(withIdentifier: "showTruck", sender: nil)
        }
    }
    
    func createRun(cell: TruckTableViewCell) {
        if let indexPath = tableView.indexPath(for: cell) {
            selectedTruck = filterSet[indexPath.row]
            showCreateRun(selectedTruck)
        }
    }
    
}
