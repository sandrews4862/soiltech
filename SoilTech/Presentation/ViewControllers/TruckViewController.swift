//
//  TruckViewController.swift
//  SoilTech
//
//  Created by Steven Andrews on 2019-10-12.
//  Copyright © 2019 Steven Andrews. All rights reserved.
//

import UIKit

class TruckViewController: STViewController {

    @IBOutlet var tableView: UITableView!
    @IBOutlet var loader: UIActivityIndicatorView!
    @IBOutlet var emptyView: UIView!
    @IBOutlet var errorLabel: UILabel!
    
    public var truck: STTruck!
    private var runs: [STRun] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = truck.truckNumber
        self.runs = truck.runs ?? []
        self.sortRuns()
        tableView.register(UINib(nibName: "RunTableViewCell", bundle: nil), forCellReuseIdentifier: "runTableViewCell")
        tableView.register(TruckTableViewHeader.self, forHeaderFooterViewReuseIdentifier: "runTableViewHeader")
        reloadRuns()
    }
    
    private func reloadRuns() {
        loader.startAnimating()
        TruckService.fetchTruck(truckId: truck.objectId, response: { [weak self] (fetchedTruck) in
            self?.loader.stopAnimating()
            if let fetchedTruck = fetchedTruck as? STTruck {
                self?.truck = fetchedTruck
                self?.runs = fetchedTruck.runs ?? []
                self?.sortRuns()
                self?.tableView.reloadData()
            }
        }) { [weak self] (fault) in
            self?.loader.stopAnimating()
            print("Error reloading truck: \(fault.debugDescription)")
        }
    }
    
    private func sortRuns() {
        self.runs = self.runs.sorted(by: { ($0.created! as Date) < ($1.created! as Date) })
    }
    
    private func showSuccessAlert() {
        reloadRuns()
    }
    
    @IBAction private func showCreateRun() {

        let alert = UIAlertController(title: "New Run", message: nil, preferredStyle: .alert)

        alert.addTextField { (textField) in
            textField.placeholder = "Run Value"
            textField.text = ""
            textField.keyboardType = .numberPad
        }

        alert.addAction(UIAlertAction(title: "Create", style: .default, handler: { [weak alert] (_) in
            let textField = alert?.textFields![0]
            if let run = textField?.text, run.isEmpty == false, run != "", let runInt = Int(run) {
                TruckService.createRun(truck: self.truck, run: runInt, response: { [weak self] (run) in
                    self?.showSuccessAlert()
                }) { (fault) in
                    print("Error creating run: \(fault.debugDescription)")
                }
            } else {
                self.showErrorAlert()
            }
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))

        self.present(alert, animated: true, completion: nil)
    }
    
    private func showErrorAlert() {
       let alert = UIAlertController(title: "Error", message: "Invalid data entered. Use numerical values only.", preferredStyle: .alert)
       alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
       self.present(alert, animated: true, completion: nil)
   }

}

extension TruckViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return runs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "runTableViewCell", for: indexPath) as? RunTableViewCell {
            let run = runs[indexPath.row]
            cell.update(run: "\(indexPath.row+1)", number: "\(run.number)")
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let action = UIContextualAction(style: .destructive, title: "Delete",
          handler: { [weak self] (action, view, completionHandler) in
            self?.loader.startAnimating()
            let run = self!.runs[indexPath.row]
            
            TruckService.deleteRun(run: run, response: { [weak self] (response) in
                self?.reloadRuns()
            }) { (fault) in
                self?.loader.stopAnimating()
                print("Error deleting run: \(fault.debugDescription)")
            }
            completionHandler(true)
        })
        let configuration = UISwipeActionsConfiguration(actions: [action])
        return configuration
    }
    
}
