//
//  ViewController.swift
//  SoilTech
//
//  Created by Steven Andrews on 2019-09-18.
//  Copyright © 2019 Steven Andrews. All rights reserved.
//

import UIKit
import Backendless

class CompaniesViewController: STViewController {
    
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var emptyView: UIView!
    @IBOutlet var errorLabel: UILabel!
    @IBOutlet var loader: UIActivityIndicatorView!
    
    private let SEARCH_DELAY = 0.3
    private var companies: [STCompany]?
    private var companiesDic: [String:STCompany] = [:]
    private var recentCompanies: [STCompany]? = []
    private var searchTimer: Timer?
    private var queryText = ""
    private var selectedCompany: STCompany?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib.init(nibName: "CompanyTableViewCell", bundle: nil), forCellReuseIdentifier: "companyTableViewCell")
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 40
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchRecentCompanies()
    }
    
    private func fetchRecentCompanies() {
        recentCompanies?.removeAll()
        companiesDic.removeAll()
        CompanyService.fetchRecentCompanies(response: { [weak self] (sheets) in
            if let sheets = sheets as? [STSheet] {
                for sheet in sheets {
                    if let company = sheet.company {
                        if (self?.companiesDic[company.objectId] == nil) {
                            self?.companiesDic[company.objectId] = company
                            self?.recentCompanies?.append(company)
                        }
                    }
                }
                self?.tableView.reloadData()
            }
        }) { (fault) in
            print("Error loading companies: \(fault.debugDescription)")
        }
    }
    
    @objc private func performSearch() {
        errorLabel.isHidden = true
        loader.startAnimating()
        CompanyService.fetchCompanies(query: queryText, response: { [weak self] (companiesData)  in
            self?.loader.stopAnimating()
            if let companiesData = companiesData as? [STCompany] {
                self?.companies = companiesData
                self?.tableView.reloadData()
                self?.emptyView.isHidden = companiesData.count != 0
            }
        }) { [weak self] (fault) in
            self?.loader.stopAnimating()
            self?.errorLabel.isHidden = false
            print("Error fetching companies: \(fault.debugDescription)")
        }
    }
    
    private func showCreateCompany() {

        let alert = UIAlertController(title: "New Company", message: "Enter the company's name", preferredStyle: .alert)

        alert.addTextField { (textField) in
            textField.placeholder = "Company Name"
            textField.text = ""
            textField.autocapitalizationType = .words
        }

        alert.addAction(UIAlertAction(title: "Create", style: .default, handler: { [weak alert] (_) in
            let textField = alert?.textFields![0]
            if let name = textField?.text, name.isEmpty == false, name != "" {
                CompanyService.createCompany(name, response: { [weak self] (company) in
                    self?.showSuccessAlert()
                    self?.selectedCompany = company as? STCompany
                    self?.fetchRecentCompanies()
                }) { (fault) in
                    print("Error creating company: \(fault.debugDescription)")
                }
            }
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))

        self.present(alert, animated: true, completion: nil)
    }
    
    private func showSuccessAlert() {
        let alert = UIAlertController(title: "Success", message: "The company has been created!", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "View", style: .default, handler: { [weak self] (action) in
            self?.performSegue(withIdentifier: "showSheet", sender: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction private func addButtonPressed() {
        showCreateCompany()
    }
    
    @IBAction private func exportButtonPressed() {
        loader.startAnimating()
        CompanyService.createExport(response: { (export) in
            self.loader.stopAnimating()
            self.showExported()
        }) { (fault) in
            self.loader.stopAnimating()
            print("Error exporting: \(fault.debugDescription)")
        }
    }
    
    private func showExported() {
        let alert = UIAlertController(title: "Success", message: "The export has been started. Check your email soon.", preferredStyle: .alert)
       alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
       self.present(alert, animated: true, completion: nil)
    }
    
    private func showExportError(error: Fault) {
        let alert = UIAlertController(title: "Success", message: "The export failed. \(error.debugDescription)", preferredStyle: .alert)
          alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
          self.present(alert, animated: true, completion: nil)
       }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showSheet" {
            if let destinaton = segue.destination as? SheetViewController {
                destinaton.company = selectedCompany
            }
        }
    }
}

extension CompaniesViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchTimer != nil {
            searchTimer?.invalidate()
            searchTimer = nil
        }
        if searchText.isEmpty || searchText == "" {
            emptyView.isHidden = true
            errorLabel.isHidden = true
            companies?.removeAll()
            companies = nil
            tableView.reloadData()
            return
        }
        queryText = searchText
        searchTimer = Timer.scheduledTimer(timeInterval: SEARCH_DELAY, target: self, selector: #selector(performSearch), userInfo: nil, repeats: false)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
}

extension CompaniesViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return companies?.count ?? recentCompanies?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let company = companies?[indexPath.row] ?? recentCompanies?[indexPath.row]
        if let company = company, let cell = tableView.dequeueReusableCell(withIdentifier: "companyTableViewCell", for: indexPath) as? CompanyTableViewCell {
            let companyName = company.name ?? "Error"
            cell.update(companyName)
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedCompany = companies?[indexPath.row] ?? recentCompanies?[indexPath.row]
        performSegue(withIdentifier: "showSheet", sender: nil)
    }
}

