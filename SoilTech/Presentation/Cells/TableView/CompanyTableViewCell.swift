//
//  CompanyTableViewCell.swift
//  SoilTech
//
//  Created by Steven Andrews on 2019-09-18.
//  Copyright © 2019 Steven Andrews. All rights reserved.
//

import UIKit

class CompanyTableViewCell: UITableViewCell {

    @IBOutlet private var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    public func update(_ title: String) {
        titleLabel.text = title
    }
    
}
