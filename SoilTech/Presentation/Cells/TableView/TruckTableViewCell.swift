//
//  TruckTableViewCell.swift
//  SoilTech
//
//  Created by Jessica Andrews on 2019-09-22.
//  Copyright © 2019 Steven Andrews. All rights reserved.
//

import UIKit

protocol TruckTableViewCellDelegate {
    func selectedListView(cell: TruckTableViewCell)
    func createRun(cell: TruckTableViewCell)
}

class TruckTableViewCell: UITableViewCell {

    @IBOutlet private var plateLabel: UILabel!
    @IBOutlet private var numberLabel: UILabel!
    @IBOutlet private var runLabel: UILabel!
    
    private var delegate: TruckTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    public func update(number: String, plate: String, runs: Int, delegate: TruckTableViewCellDelegate) {
        numberLabel.text = number
        plateLabel.text = plate
        runLabel.text = "\(runs)"
        self.delegate = delegate
    }
    
    @IBAction private func selectedList() {
        delegate?.selectedListView(cell: self)
    }
    
    @IBAction private func selectedCell() {
        delegate?.createRun(cell: self)
    }
    
}
