//
//  RunTableViewCell.swift
//  SoilTech
//
//  Created by Steven Andrews on 2019-10-12.
//  Copyright © 2019 Steven Andrews. All rights reserved.
//

import UIKit

class RunTableViewCell: UITableViewCell {

    @IBOutlet private var runLabel: UILabel!
    @IBOutlet private var runNumberLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    public func update(run: String, number: String) {
        runLabel.text = "Run \(run)"
        runNumberLabel.text = "\(number)"
    }
    
}
