//
//  TruckTableViewHeader.swift
//  SoilTech
//
//  Created by Jessica Andrews on 2019-09-22.
//  Copyright © 2019 Steven Andrews. All rights reserved.
//

import UIKit

class TruckTableViewHeader: UITableViewHeaderFooterView {

    let truckNumberTitle: UILabel = {
        var label = UILabel()
        label.text = "Truck Number"
        label.font = UIFont(name: "Helvetica", size: 14)
        label.textColor = UIColor.gray
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    let plateLabel: UILabel = {
        var label = UILabel()
        label.text = "License Plate"
        label.font = UIFont(name: "Helvetica", size: 14)
        label.textColor = UIColor.gray
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    let runsLabel: UILabel = {
        var label = UILabel()
        label.text = "Runs"
        label.font = UIFont(name: "Helvetica", size: 14)
        label.textColor = UIColor.gray
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()

    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        
        contentView.backgroundColor = UIColor(named: "background")
        
        contentView.addSubview(truckNumberTitle)
        contentView.addSubview(plateLabel)
        contentView.addSubview(runsLabel)
        
        addConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addConstraints() {
        NSLayoutConstraint.activate([
            truckNumberTitle.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            truckNumberTitle.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            plateLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 180),
            plateLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            runsLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 320),
            runsLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
        ])
    }
    
}
