//
//  Environment.swift
//  TopDish
//
//  Created by Steven Andrews on 2018-06-26.
//  Copyright © 2018 Steven Andrews. All rights reserved.
//

import UIKit

public enum Environment: Int {
    case cloud
    case dev
    case prod
}

class Config: NSObject {

    public static let environment: Environment = .prod
    
}
