//
//  AppDelegate.swift
//  SoilTech
//
//  Created by Steven Andrews on 2019-09-18.
//  Copyright © 2019 Steven Andrews. All rights reserved.
//

import UIKit
import IQKeyboardManager
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window : UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        IQKeyboardManager.shared().isEnabled = true
        STBackendlessManager.startBackendlessWith(Networking.backendlessConfig)
        let filePath = Bundle.main.path(forResource: "GoogleService-Info", ofType: "plist")!
        let options = FirebaseOptions(contentsOfFile: filePath)
        FirebaseApp.configure(options: options!)
        return true
    }

}

