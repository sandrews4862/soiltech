//
//  TDBaseModel.swift
//  TopDish
//
//  Created by Steven Andrews on 2018-06-26.
//  Copyright © 2018 Steven Andrews. All rights reserved.
//

import UIKit

@objcMembers class STBaseModel: NSObject {
    public var objectId: String!
    public var created: NSDate!
    public var updated: NSDate?
}
