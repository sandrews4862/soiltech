//
//  TDImage.swift
//  TopDish
//
//  Created by Steven Andrews on 2018-06-26.
//  Copyright © 2018 Steven Andrews. All rights reserved.
//

import UIKit

@objcMembers class STSheet: STBaseModel {
    
    public var site: String?
    public var tickets: Bool = false
    public var trucks: [STTruck]?
    public var company: STCompany!
    
}
