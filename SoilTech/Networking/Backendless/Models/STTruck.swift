//
//  TDDishReview.swift
//  TopDish
//
//  Created by Steven Andrews on 2018-06-26.
//  Copyright © 2018 Steven Andrews. All rights reserved.
//

import UIKit

@objcMembers class STTruck: STBaseModel {
    
    public var truckNumber: String?
    public var plate: String?
    public var runs: [STRun]?
    
}
