//
//  TDBackendlessConfig.swift
//  TopDish
//
//  Created by Steven Andrews on 2018-06-26.
//  Copyright © 2018 Steven Andrews. All rights reserved.
//

import UIKit

class STBackendlessConfig: NSObject {
    
    public var backendlessUrl: String? //optional, override if using cloud
    public var backendlessAppId: String?
    public var backendlessIosSecret: String?
    
    public init(url: String? = nil, appId: String, secret: String) {
        
        self.backendlessUrl       = url
        self.backendlessAppId     = appId
        self.backendlessIosSecret = secret
    }
}
