//
//  TDBackendlessManager.swift
//  TopDish
//
//  Created by Steven Andrews on 2019-02-18.
//  Copyright © 2019 Steven Andrews. All rights reserved.
//


import Backendless

class STBackendlessManager: NSObject {
    
    static let backendless = Backendless.shared
    
    public static func startBackendlessWith(_ config: STBackendlessConfig) {
        if let url = config.backendlessUrl {
            backendless.hostUrl = url
        }
        backendless.initApp(applicationId: config.backendlessAppId!, apiKey: config.backendlessIosSecret!)
        mapModelsToTables()
        print("Backendless Started")
    }
    
    private static func mapModelsToTables() {
        backendless.data.of(STCompany.self).mapToTable(tableName: "Company")
        backendless.data.of(STSheet.self).mapToTable(tableName: "Sheet")
        backendless.data.of(STRun.self).mapToTable(tableName: "Run")
        backendless.data.of(STTruck.self).mapToTable(tableName: "Truck")
        backendless.data.of(STExport.self).mapToTable(tableName: "Export")
    }
    
}
