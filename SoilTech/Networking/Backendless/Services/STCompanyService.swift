//
//  TDRestaurantService.swift
//  TopDish
//
//  Created by Steven Andrews on 2018-10-17.
//  Copyright © 2018 Steven Andrews. All rights reserved.
//

import UIKit
import Backendless

class CompanyService: NSObject {

    //MARK: - Constants
    static let PAGE_SIZE: Int = 20
    
    //MARK: - Relations
    static let RELATION_COMPANY = "company"
    static let RELATION_TRUCKS = "trucks"
    static let RELATION_TRUCKS_RUNS = "trucks.runs"
    
    public static func fetchCompanies(query: String, page: Int = 0, response: @escaping (([Any]?) -> Void), error: @escaping ((Fault) -> Void)) {
        let whereClause = "name LIKE '%\(query)%'"
        let query = DataQueryBuilder()
        query.setWhereClause(whereClause: whereClause)
        
        let dataStore = Backendless.shared.data.of(STCompany.self)
        dataStore.find(queryBuilder: query, responseHandler: response, errorHandler: error)
    }
    
    public static func fetchRecentCompanies(response: @escaping (([Any]?) -> Void), error: @escaping ((Fault) -> Void)) {
        let query = DataQueryBuilder()
        query.setSortBy(sortBy: ["updated DESC, created DESC"])
        query.setPageSize(pageSize: 20)
        query.setRelated(related: [RELATION_COMPANY])
        
        let dataStore = Backendless.shared.data.of(STSheet.self)
        dataStore.find(queryBuilder: query, responseHandler: response, errorHandler: error)
    }
    
    public static func createCompany(_ name: String, response: @escaping ((Any?) -> Void), error: @escaping ((Fault) -> Void)) {
        let newCompany = STCompany()
        newCompany.name = name
        let dataStore = Backendless.shared.data.of(STCompany.self)
        dataStore.save(entity: newCompany, responseHandler: response, errorHandler: error)
    }
    
    public static func createSheet(site: String, company: STCompany, response: @escaping ((Any?) -> Void), error: @escaping ((Fault) -> Void)) {
        let newSheet = STSheet()
        newSheet.site = site
        newSheet.company = company
        let dataStore = Backendless.shared.data.of(STSheet.self)
        dataStore.save(entity: newSheet, responseHandler: { (sheet) in
            if let sheet = sheet as? STSheet {
                dataStore.addRelation(columnName: "company", parentObjectId: sheet.objectId, childrenObjectIds: [company.objectId], responseHandler: response, errorHandler: error)
            }
        }, errorHandler: error)
    }
    
    public static func fetchCompany(companyId: String, response: @escaping ((Any) -> Void), error: @escaping ((Fault) -> Void)) {
        let whereClause = "objectId = '\(companyId)'"
        let query = DataQueryBuilder()
        query.setWhereClause(whereClause: whereClause)
        query.setPageSize(pageSize: PAGE_SIZE)
        
        let dataStore = Backendless.shared.data.of(STCompany.self)
        dataStore.find(queryBuilder: query, responseHandler: response, errorHandler: error)
    }
    
    public static func fetchMostRecentSheet(companyId: String, response: @escaping ((Any) -> Void), error: @escaping ((Fault) -> Void)) {
        let whereClause = "company.objectId = '\(companyId)'"
        let query = DataQueryBuilder()
        query.setWhereClause(whereClause: whereClause)
        query.setRelated(related: [RELATION_TRUCKS, RELATION_TRUCKS_RUNS])
        query.setPageSize(pageSize: 1)
        query.setSortBy(sortBy: ["created DESC"])
        
        let dataStore = Backendless.shared.data.of(STSheet.self)
        dataStore.find(queryBuilder: query, responseHandler: response, errorHandler: error)
    }
    
    public static func fetchSheet(sheetId: String, response: @escaping ((Any) -> Void), error: @escaping ((Fault) -> Void)) {
        let query = DataQueryBuilder()
        query.setPageSize(pageSize: 100)
        query.setRelated(related: [RELATION_TRUCKS, RELATION_TRUCKS_RUNS])
        query.setSortBy(sortBy: ["created DESC"])
        
        let dataStore = Backendless.shared.data.of(STSheet.self)
        dataStore.findById(objectId: sheetId, queryBuilder: query, responseHandler: response, errorHandler: error)
    }
    
    public static func createExport(response: @escaping ((Any?) -> Void), error: @escaping ((Fault) -> Void)) {
       let newExport = STExport()
       let dataStore = Backendless.shared.data.of(STExport.self)
       dataStore.save(entity: newExport, responseHandler: response, errorHandler: error)
   }
    
}
