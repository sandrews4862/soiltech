//
//  TDRestaurantService.swift
//  TopDish
//
//  Created by Steven Andrews on 2018-10-17.
//  Copyright © 2018 Steven Andrews. All rights reserved.
//

import UIKit
import Backendless

class TruckService: NSObject {

    //MARK: - Constants
    static let PAGE_SIZE: Int = 20
    
    //MARK: - Relations
    static let RELATION_RUNS = "runs"
    
    public static func createTruck(number: String, plate: String, sheet: STSheet, response: @escaping ((STTruck?) -> Void), error: @escaping ((Fault) -> Void)) {
        let newTruck = STTruck()
        newTruck.truckNumber = number
        newTruck.plate = plate
        let dataStore = Backendless.shared.data.of(STTruck.self)
        dataStore.save(entity: newTruck, responseHandler: { (createdTruck) in
            if let createdTruck = createdTruck as? STTruck {
                let sheetStore = Backendless.shared.data.of(STSheet.self)
                sheetStore.addRelation(columnName: "trucks", parentObjectId: sheet.objectId, childrenObjectIds: [createdTruck.objectId], responseHandler: { (status) in
                    response(createdTruck)
                }, errorHandler: error)
            }
        }, errorHandler: error)
    }
    
    public static func createRun(truck: STTruck, run: Int, response: @escaping ((Any?) -> Void), error: @escaping ((Fault) -> Void)) {
        let newRun = STRun()
        newRun.number = run
        let dataStore = Backendless.shared.data.of(STRun.self)
        dataStore.save(entity: newRun, responseHandler: { (createdRun) in
            if let createdRun = createdRun as? STRun {
                let truckStore = Backendless.shared.data.of(STTruck.self)
                truckStore.addRelation(columnName: "runs", parentObjectId: truck.objectId, childrenObjectIds: [createdRun.objectId], responseHandler: response, errorHandler: error)
            }
        }, errorHandler: error)
    }
    
    public static func fetchTruck(truckId: String, response: @escaping ((Any?) -> Void), error: @escaping ((Fault) -> Void)) {
        let query = DataQueryBuilder()
        query.setRelated(related: [RELATION_RUNS])

        let dataStore = Backendless.shared.data.of(STTruck.self)
        dataStore.findById(objectId: truckId, queryBuilder: query, responseHandler: response, errorHandler: error)
    }
    
    public static func deleteRun(run: STRun, response: @escaping ((Any?) -> Void), error: @escaping ((Fault) -> Void)) {
        let dataStore = Backendless.shared.data.of(STRun.self)
        dataStore.remove(entity: run, responseHandler: response, errorHandler: error)
    }
    
    public static func deleteTruck(truck: STTruck, response: @escaping ((Any?) -> Void), error: @escaping ((Fault) -> Void)) {
        let dataStore = Backendless.shared.data.of(STTruck.self)
        dataStore.remove(entity: truck, responseHandler: response, errorHandler: error)
    }
    
    public static func fetchRunCount(truck: STTruck, response: @escaping ((Any?) -> Void), error: @escaping ((Fault) -> Void)) {
        let dataStore = Backendless.shared.data.of(STTruck.self)
        dataStore.getObjectCount(responseHandler: response, errorHandler: error)
    }
    
}


