//
//  Networking.swift
//  TopDish
//
//  Created by Steven Andrews on 2018-06-26.
//  Copyright © 2018 Steven Andrews. All rights reserved.
//

import UIKit

//dev
public let backendlessURLDev: String? = nil
public var backendlessAppIdDev: String = ""
public var backendlessIosSecretDev: String = ""

//Cloud
public let backendlessURLCloud: String? = "http://207.246.125.95/api"
public let backendlessAppIdCloud: String = "EE2B6A28-9DBD-F265-FFA6-AEBE1B8B5D00"
public let backendlessIosSecretCloud: String = "7B32C01F-CDFF-E752-FF44-AFE7D56F7F00"

//Prod
public let backendlessURLProd: String? = nil
public var backendlessAppIdProd: String = ""
public var backendlessIosSecretProd: String = ""

class Networking: NSObject {

    public static var backendlessConfig: STBackendlessConfig {
        get {
            switch Config.environment {
            case .cloud:
                return STBackendlessConfig(url: backendlessURLCloud, appId: backendlessAppIdCloud, secret: backendlessIosSecretCloud)
            case .dev:
                if let path = Bundle.main.path(forResource: "Info", ofType: "plist") , let dic = NSDictionary(contentsOfFile: path), let appId = dic["BACKENDLESS_APP_ID"] as? String, let secret = dic["BACKENDLESS_IOS_SECRET"] as? String {
                    //Environment
                    backendlessAppIdDev = appId
                    backendlessIosSecretDev = secret
                }
                return STBackendlessConfig(url: backendlessURLDev, appId: backendlessAppIdDev, secret: backendlessIosSecretDev)
            case .prod:
                if let path = Bundle.main.path(forResource: "Info", ofType: "plist") , let dic = NSDictionary(contentsOfFile: path), let appId = dic["BACKENDLESS_APP_ID"] as? String, let secret = dic["BACKENDLESS_IOS_SECRET"] as? String {
                    //Environment
                    backendlessAppIdProd = appId
                    backendlessIosSecretProd = secret
                }
                return STBackendlessConfig(url: backendlessURLProd, appId: backendlessAppIdProd, secret: backendlessIosSecretProd)
            }
        }
    }
    
}
