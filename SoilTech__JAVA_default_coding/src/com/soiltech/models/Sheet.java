package com.soiltech.models;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.geo.GeoPoint;

public class Sheet
{
  private Company company;
  private java.util.List<Truck> trucks;
  private java.util.Date updated;
  private java.util.Date created;
  private Boolean tickets;
  private String site;
  private String ownerId;
  private String objectId;

  public Company getCompany()
  {
    return this.company;
  }

  public java.util.List<Truck> getTrucks()
  {
    return this.trucks;
  }

  public java.util.Date getUpdated()
  {
    return this.updated;
  }

  public java.util.Date getCreated()
  {
    return this.created;
  }

  public Boolean getTickets()
  {
    return this.tickets;
  }

  public String getSite()
  {
    return this.site;
  }

  public String getOwnerId()
  {
    return this.ownerId;
  }

  public String getObjectId()
  {
    return this.objectId;
  }


  public void setCompany( Company company )
  {
    this.company = company;
  }

  public void setTrucks( java.util.List<Truck> trucks )
  {
    this.trucks = trucks;
  }

  public void setUpdated( java.util.Date updated )
  {
    this.updated = updated;
  }

  public void setCreated( java.util.Date created )
  {
    this.created = created;
  }

  public void setTickets( Boolean tickets )
  {
    this.tickets = tickets;
  }

  public void setSite( String site )
  {
    this.site = site;
  }

  public void setOwnerId( String ownerId )
  {
    this.ownerId = ownerId;
  }

  public void setObjectId( String objectId )
  {
    this.objectId = objectId;
  }

  public Sheet save()
  {
    return Backendless.Data.of( Sheet.class ).save( this );
  }

  public Long remove()
  {
    return Backendless.Data.of( Sheet.class ).remove( this );
  }

  public static Sheet findById( String id )
  {
    return Backendless.Data.of( Sheet.class ).findById( id );
  }

  public static Sheet findFirst()
  {
    return Backendless.Data.of( Sheet.class ).findFirst();
  }

  public static Sheet findLast()
  {
    return Backendless.Data.of( Sheet.class ).findLast();
  }
}