package com.soiltech.models;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.geo.GeoPoint;

public class Export
{
  private String ownerId;
  private String objectId;
  private Integer number;
  private java.util.Date updated;
  private java.util.Date created;

  public String getOwnerId()
  {
    return this.ownerId;
  }

  public String getObjectId()
  {
    return this.objectId;
  }

  public java.util.Date getUpdated()
  {
    return this.updated;
  }

  public java.util.Date getCreated()
  {
    return this.created;
  }


  public void setOwnerId( String ownerId )
  {
    this.ownerId = ownerId;
  }

  public void setObjectId( String objectId )
  {
    this.objectId = objectId;
  }

  public void setUpdated( java.util.Date updated )
  {
    this.updated = updated;
  }

  public void setCreated( java.util.Date created )
  {
    this.created = created;
  }

  public Export save()
  {
    return Backendless.Data.of( Export.class ).save( this );
  }

  public Long remove()
  {
    return Backendless.Data.of( Export.class ).remove( this );
  }

  public static Export findById( String id )
  {
    return Backendless.Data.of( Export.class ).findById( id );
  }

  public static Export findFirst()
  {
    return Backendless.Data.of( Export.class ).findFirst();
  }

  public static Export findLast()
  {
    return Backendless.Data.of( Export.class ).findLast();
  }

  public void setNumber( Integer number )
{
  this.number = number;
}

  public Integer getNumber()
  {
   return this.number;
  }
}