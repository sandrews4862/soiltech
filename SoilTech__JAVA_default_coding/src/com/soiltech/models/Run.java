package com.soiltech.models;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.geo.GeoPoint;

public class Run
{
  private java.util.Date updated;
  private String ownerId;
  private java.util.Date created;
  private String objectId;
  private Integer number;

  public java.util.Date getUpdated()
  {
    return this.updated;
  }

  public String getOwnerId()
  {
    return this.ownerId;
  }

  public java.util.Date getCreated()
  {
    return this.created;
  }

  public String getObjectId()
  {
    return this.objectId;
  }

  public Integer getNumber()
  {
    return this.number;
  }


  public void setUpdated( java.util.Date updated )
  {
    this.updated = updated;
  }

  public void setOwnerId( String ownerId )
  {
    this.ownerId = ownerId;
  }

  public void setCreated( java.util.Date created )
  {
    this.created = created;
  }

  public void setObjectId( String objectId )
  {
    this.objectId = objectId;
  }

  public void setNumber( Integer number )
  {
    this.number = number;
  }

  public Run save()
  {
    return Backendless.Data.of( Run.class ).save( this );
  }

  public Long remove()
  {
    return Backendless.Data.of( Run.class ).remove( this );
  }

  public static Run findById( String id )
  {
    return Backendless.Data.of( Run.class ).findById( id );
  }

  public static Run findFirst()
  {
    return Backendless.Data.of( Run.class ).findFirst();
  }

  public static Run findLast()
  {
    return Backendless.Data.of( Run.class ).findLast();
  }
}