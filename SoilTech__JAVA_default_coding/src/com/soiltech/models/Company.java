package com.soiltech.models;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.geo.GeoPoint;

public class Company
{
  private String objectId;
  private String name;
  private java.util.Date updated;
  private String ownerId;
  private java.util.Date created;

  public String getObjectId()
  {
    return this.objectId;
  }

  public String getName()
  {
    return this.name;
  }

  public java.util.Date getUpdated()
  {
    return this.updated;
  }

  public String getOwnerId()
  {
    return this.ownerId;
  }

  public java.util.Date getCreated()
  {
    return this.created;
  }


  public void setObjectId( String objectId )
  {
    this.objectId = objectId;
  }

  public void setName( String name )
  {
    this.name = name;
  }

  public void setUpdated( java.util.Date updated )
  {
    this.updated = updated;
  }

  public void setOwnerId( String ownerId )
  {
    this.ownerId = ownerId;
  }

  public void setCreated( java.util.Date created )
  {
    this.created = created;
  }

  public Company save()
  {
    return Backendless.Data.of( Company.class ).save( this );
  }

  public Long remove()
  {
    return Backendless.Data.of( Company.class ).remove( this );
  }

  public static Company findById( String id )
  {
    return Backendless.Data.of( Company.class ).findById( id );
  }

  public static Company findFirst()
  {
    return Backendless.Data.of( Company.class ).findFirst();
  }

  public static Company findLast()
  {
    return Backendless.Data.of( Company.class ).findLast();
  }
}