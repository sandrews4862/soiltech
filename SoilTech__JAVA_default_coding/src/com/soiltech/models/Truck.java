package com.soiltech.models;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.geo.GeoPoint;

public class Truck
{
  private java.util.List<Run> runs;
  private java.util.Date created;
  private java.util.Date updated;
  private String truckNumber;
  private String ownerId;
  private String objectId;
  private String plate;

  public java.util.List<Run> getRuns()
  {
    return this.runs;
  }

  public java.util.Date getCreated()
  {
    return this.created;
  }

  public java.util.Date getUpdated()
  {
    return this.updated;
  }

  public String getTruckNumber()
  {
    return this.truckNumber;
  }

  public String getOwnerId()
  {
    return this.ownerId;
  }

  public String getObjectId()
  {
    return this.objectId;
  }

  public String getPlate()
  {
    return this.plate;
  }


  public void setRuns( java.util.List<Run> runs )
  {
    this.runs = runs;
  }

  public void setCreated( java.util.Date created )
  {
    this.created = created;
  }

  public void setUpdated( java.util.Date updated )
  {
    this.updated = updated;
  }

  public void setTruckNumber( String truckNumber )
  {
    this.truckNumber = truckNumber;
  }

  public void setOwnerId( String ownerId )
  {
    this.ownerId = ownerId;
  }

  public void setObjectId( String objectId )
  {
    this.objectId = objectId;
  }

  public void setPlate( String plate )
  {
    this.plate = plate;
  }

  public Truck save()
  {
    return Backendless.Data.of( Truck.class ).save( this );
  }

  public Long remove()
  {
    return Backendless.Data.of( Truck.class ).remove( this );
  }

  public static Truck findById( String id )
  {
    return Backendless.Data.of( Truck.class ).findById( id );
  }

  public static Truck findFirst()
  {
    return Backendless.Data.of( Truck.class ).findFirst();
  }

  public static Truck findLast()
  {
    return Backendless.Data.of( Truck.class ).findLast();
  }
}