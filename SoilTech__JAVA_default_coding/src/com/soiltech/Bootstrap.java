
package com.soiltech;

import com.backendless.Backendless;
import com.backendless.servercode.IBackendlessBootstrap;

import com.soiltech.models.Export;

public class Bootstrap implements IBackendlessBootstrap
{
            
  @Override
  public void onStart()
  {

    Backendless.Persistence.mapTableToClass( "Export", Export.class );
    // add your code here
  }
    
  @Override
  public void onStop()
  {
    // add your code here
  }
    
}
        