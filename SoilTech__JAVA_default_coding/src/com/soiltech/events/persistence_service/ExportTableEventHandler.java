package com.soiltech.events.persistence_service;

import com.backendless.Backendless;
import com.backendless.geo.GeoPoint;
import com.backendless.persistence.BackendlessDataQuery;
import com.backendless.persistence.DataQueryBuilder;
import com.backendless.property.ObjectProperty;
import com.backendless.servercode.ExecutionResult;
import com.backendless.servercode.RunnerContext;
import com.backendless.servercode.annotation.Asset;
import com.backendless.servercode.annotation.Async;

import com.soiltech.models.Export;
import com.soiltech.models.Run;
import com.soiltech.models.Sheet;
import com.soiltech.models.Truck;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
* ExportTableEventHandler handles events only for the "Export" table. This is accomplished
* with the @Asset( "Export" ) annotation. 
* The methods in the class correspond to the events selected in Backendless
* Console.
*/
    
@Asset( "Export" )
public class ExportTableEventHandler extends com.backendless.servercode.extension.PersistenceExtender<Export>
{

  @Override
  public void afterCreate( RunnerContext context, Export export, ExecutionResult<Export> result ) throws Exception
  {
    Date today = Calendar.getInstance().getTime();
    DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
    String strDate = dateFormat.format(today);

    String whereClause = "created >= '"+strDate+" 00:00:00'";
    DataQueryBuilder queryBuilder = DataQueryBuilder.create();
    queryBuilder.setWhereClause( whereClause );
    queryBuilder.setRelated("trucks", "trucks.runs", "company");
    queryBuilder.setPageSize(100);

    List<Sheet> sheets = Backendless.Data.of( Sheet.class).find( queryBuilder );
    System.out.println("FOUND: "+sheets.size()+" sheets.");
    Export newExport = new Export();
    newExport.setNumber(sheets.size());
    newExport.save();

    for (Sheet sheet : sheets) {
      System.out.println(sheet.getSite());
      for (Truck truck : sheet.getTrucks()) {
        System.out.println(truck.getPlate());
        for (Run run : truck.getRuns()) {
          System.out.println(run.getNumber());
        }
      }
    }

  }
    
}
        